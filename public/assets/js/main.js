$(document).ready(function(){
  $('.scroller').click(function(){
    $('html, body').animate({
        scrollTop: $( $(this).attr('href') ).offset().top - 92
    }, 500);
    return false;
  });
});

$(window).scroll(function() {
    var scroll = $(window).scrollTop();
    if (scroll >= 92) {
        $(".navbar").addClass("fixed");
    } else {
        $(".navbar").removeClass("fixed");
    }
});
