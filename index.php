<?php
  require_once 'vendor/autoload.php';

  if (isset($_GET['controller']) && isset($_GET['action'])) {
    $controller = $_GET['controller'];
    $action     = $_GET['action'];
  } else {
    $controller = 'pages';
    $action     = 'home';
  }

  $production = false;
  $project_slug = 'template-php';
  $ga = '';
  $gtm = '';

  if ($production == true) {
      define('URL', 'http://'.$_SERVER['SERVER_NAME'].'/');

      // database connection
      // require_once 'db/production.php';

      // enviroment
      define('ENV', 'production');
  } else {
    define('URL', 'http://'.$_SERVER['SERVER_NAME'].'/'.$project_slug.'/' );

    // database connection
    // require_once 'db/development.php';

    // enviroment
    define('ENV', 'development');
  }

  define('SITE', URL);
  define('CSS', URL.'public/assets/css/');
  define('JS', URL.'public/assets/js/');
  define('IMG', URL.'public/assets/img/');
  define('VID', URL.'public/assets/video/');

  require_once 'views/layout.php';
?>
